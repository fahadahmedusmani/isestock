//
//  SecurityNetworkService.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

class SecurityNetworkService: NSObject{
    
    static let requestManager = RequestManager.shared
    
    private override init() {}
    
    public static func startHandshake(_ params: StartHandshakeRequest,completion: @escaping (_ response: StartHandshakeResponse?, _ error: String?) -> Void){
        
        let request = RequestBuilder.shared.getRequestForStartHandshake(params)
        
        requestManager.performRequest(request: request) { (result: Result<StartHandshakeResponse>) in
            
            switch result{
            
            case .success(let response):
                
                if response.status?.isSuccess ?? false{
                    //return success response
                    completion(response, nil)
                }
                else{
                    //return error incase of failure response
                    completion(nil, response.status?.error?["message"] as? String ?? "")
                }
                
            case .failure(let error):
                completion(nil, error.localizedDescription)
            }
        }
    }
}
