//
//  RequestManager.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation
import Alamofire

protocol RequestProcessorDelegate{
    func processRequest(_ request: Request) -> Void
}

enum Result<T> {
    case success(T)
    case failure(Error)
}

class RequestManager : NSObject{
    
    static let timeoutInterval = 60.0 //seconds
    
    // use shared manager.
    static let shared = RequestManager()
    
    fileprivate override init() {}
    
    fileprivate let sessionManager: Alamofire.Session = {
        return getSession()
    }()
    
    fileprivate static func getSession() -> Alamofire.Session{
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = RequestManager.timeoutInterval
        configuration.urlCache = nil
        return Session(configuration: configuration)
    }
    
    func performRequest<T: Codable>(request: Request, completion: @escaping (_ response: Result<T>) -> Void){
        
        for processor  in request.preprocessors {
            (processor as? RequestProcessorDelegate)?.processRequest(request)
        }
        
        print(request.url!)
        print(request.header)
        print(request.body ?? [:])
        
        self.sessionManager.request(request.url, method: request.httpMethod, parameters: request.body as? Parameters, encoding: JSONEncoding.default, headers: request.header).responseJSON(completionHandler: { (response) in
            
            print(response)
            
            switch response.result{
            case .success:
                
                //IF SERVICE IS DOWN THEN SHOW GENERIC ERROR
                if response.response?.statusCode == 500{
                    let serviceDownResponse = BaseResponse.init(status: Status.init(isSuccess: false, error: ["code":0, "message": Title.requestFailed]))
                    completion(Result.success(serviceDownResponse as! T))
                    return
                }
                
                if let result = try? JSONDecoder().decode(T.self, from: response.data!) {
                    completion((Result.success(result)))
                }
                
            case .failure(let error):
                completion(Result.failure(error))
            }
        })
    }
}

