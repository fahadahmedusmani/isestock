//
//  GetStockListRequest.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation

struct GetStockListRequest: Codable{
    
    var period : String?
}

