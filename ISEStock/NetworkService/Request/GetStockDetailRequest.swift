//
//  GetStockDetailRequest.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import Foundation

struct GetStockDetailRequest: Codable{
    
    var id : String?
}

