//
//  Request.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit
import Alamofire

class Request {

    var httpMethod: HTTPMethod!
    var url: URL!
    var path: String!
    var header: HTTPHeaders = [:]
    var body: Any!
    var response: Any!
    var preprocessors: NSMutableArray = NSMutableArray()
    var postprocessors: NSMutableArray = NSMutableArray()
    
    fileprivate init() {
    }
    
    public init(httpMethod: HTTPMethod, url: URL, path: String) {
        self.httpMethod = httpMethod
        self.url = url
        self.path = path
    }
}

