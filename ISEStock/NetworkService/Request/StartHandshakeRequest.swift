//
//  StartHandshakeRequest.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

struct StartHandshakeRequest: Codable {
  
    var deviceId : String?
    var systemVersion : String?
    var platformName : String?
    var deviceModel : String?
    var manifacturer : String?
  
}
