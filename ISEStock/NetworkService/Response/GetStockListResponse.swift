//
//  GetStockListResponse.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation

class GetStockListResponse: BaseResponse{
    
    var stocks : [Stock]?
    
    enum CodingKeys: String, CodingKey {
        case stocks
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        stocks = try values.decodeIfPresent([Stock].self, forKey: .stocks)
    }
}
