//
//  StartHandshakeResponse.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

class StartHandshakeResponse: BaseResponse{
    
      var aesKey : String?
      var aesIV : String?
      var authorization : String?
      var lifeTime : String?
   
    enum CodingKeys: String, CodingKey {
        case aesKey
        case aesIV
        case authorization
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        aesKey = try values.decodeIfPresent(String.self, forKey: .aesKey)
        aesIV = try values.decodeIfPresent(String.self, forKey: .aesIV)
        authorization = try values.decodeIfPresent(String.self, forKey: .authorization)
    }
    
}
