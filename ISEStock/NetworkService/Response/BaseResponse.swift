//
//  BaseResponse.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

class BaseResponse: Codable {
    
    var status : Status?
    
    enum CodingKeys: String, CodingKey {
        case status
    }
    
    init(status: Status) {
        self.status = status
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Status.self, forKey: .status)
    }
    
    func encode(to encoder: Encoder) throws {}
}


class Status: Codable {
    
    var isSuccess: Bool?
    var error: [String: Any]?
    
    enum CodingKeys: String, CodingKey {
        case isSuccess
        case error
    }
    
    init(isSuccess: Bool, error: [String: Any]) {}
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool.self, forKey: .isSuccess)
        error = try values.decodeIfPresent([String: Any].self, forKey: .error)
    }
    
    func encode(to encoder: Encoder) throws {}
}
