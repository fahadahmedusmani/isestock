//
//  StockNetworkService.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation

class StockNetworkService: NSObject{
    
    static let requestManager = RequestManager.shared
    
    private override init() {}
    
    public static func getStockList(_ params: GetStockListRequest,completion: @escaping (_ response: GetStockListResponse?, _ error: String?) -> Void){
        
        let request = RequestBuilder.shared.getRequestForStockList(params)
        
        requestManager.performRequest(request: request) { (result: Result<GetStockListResponse>) in
            
            switch result{
            
            case .success(let response):
                
                if response.status?.isSuccess ?? false{
                    //return success response
                    completion(response, nil)
                }
                else{
                    //return error incase of failure response
                    completion(nil, response.status?.error?["message"] as? String ?? "")
                }
                
            case .failure(let error):
                completion(nil, error.localizedDescription)
            }
        }
    }
    
    
    public static func getStockDetail(_ params: GetStockDetailRequest,completion: @escaping (_ response: GetStockDetailResponse?, _ error: String?) -> Void){
        
        let request = RequestBuilder.shared.getRequestForStockDetail(params)
        
        requestManager.performRequest(request: request) { (result: Result<GetStockDetailResponse>) in
            
            switch result{
            
            case .success(let response):
                
                if response.status?.isSuccess ?? false{
                    //return success response
                    completion(response, nil)
                }
                else{
                    //return error incase of failure response
                    completion(nil, response.status?.error?["message"] as? String ?? "")
                }
                
            case .failure(let error):
                completion(nil, error.localizedDescription)
            }
        }
    }
}
