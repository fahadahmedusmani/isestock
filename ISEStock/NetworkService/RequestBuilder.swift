//
//  RequestBuilder.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation
import Alamofire

class RequestBuilder: NSObject {
    
    static var shared = RequestBuilder()
    let baseUrl = try! URLConstants.getBaseUrl().asURL()
    
    // MARK:- Handshake Request
    func getRequestForStartHandshake(_ params: StartHandshakeRequest) -> Request{
       
        let requestPath = URLConversion.decryptURL(from: URLConstants.EndPoints.startHandshake) ?? ""
       
        let request = Request(httpMethod: .post, url: baseUrl.appendingPathComponent(requestPath), path: requestPath)
        
        request.body = try? JSONSerialization.jsonObject(with: JSONEncoder().encode(params), options: []) as? [String: Any]
        
        request.preprocessors.add(GenericRequestPreprocessor() as AnyObject)
        return request
    }
    
    // MARK:- Get Stock List Request
    func getRequestForStockList(_ params: GetStockListRequest) -> Request{
       
        let requestPath = URLConversion.decryptURL(from: URLConstants.EndPoints.getStockList) ?? ""
       
        let request = Request(httpMethod: .post, url: baseUrl.appendingPathComponent(requestPath), path: requestPath)
        
        request.body = try? JSONSerialization.jsonObject(with: JSONEncoder().encode(params), options: []) as? [String: Any]
        request.preprocessors.add(GenericRequestPreprocessor() as AnyObject)
        request.preprocessors.add(AuthPreprocessor() as AnyObject)
        request.preprocessors.add(AESPreprocessor() as AnyObject)
        return request
    }
    
    // MARK:- Get Stock Detail Request
    func getRequestForStockDetail(_ params: GetStockDetailRequest) -> Request{
       
        let requestPath = URLConversion.decryptURL(from: URLConstants.EndPoints.getStockDetail) ?? ""
       
        let request = Request(httpMethod: .post, url: baseUrl.appendingPathComponent(requestPath), path: requestPath)
        
        request.body = try? JSONSerialization.jsonObject(with: JSONEncoder().encode(params), options: []) as? [String: Any]
        request.preprocessors.add(GenericRequestPreprocessor() as AnyObject)
        request.preprocessors.add(AuthPreprocessor() as AnyObject)
        request.preprocessors.add(AESPreprocessor() as AnyObject)
        return request
    }
}
