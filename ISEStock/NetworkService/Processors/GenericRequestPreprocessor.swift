//
//  GenericRequestPreprocessor.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation


class GenericRequestPreprocessor: RequestProcessorDelegate{
    
    func processRequest(_ request: Request) {
        request.header["Content-Type"] = "application/json"
        request.header["POST"] = request.path
    }
}
