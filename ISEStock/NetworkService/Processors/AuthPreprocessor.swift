//
//  AuthPreprocessor.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation


class AuthPreprocessor: RequestProcessorDelegate{
    
    func processRequest(_ request: Request) {
        request.header["X-VP-Authorization"] = SessionManager.shared.getAuthorization()
    }
}
