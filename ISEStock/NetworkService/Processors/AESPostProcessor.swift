//
//  AESPostProcessor.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation
import CryptoSwift

class AESPostProcessor: NSObject, RequestProcessorDelegate{
  
    //use this method for full payload decryption
    func processRequest(_ request: Request) {}
 
}

extension String{
    
    func aesDecrypt(keyBytes: [UInt8], blockMode: BlockMode ) throws -> String {
        
        if let data = Data(base64Encoded: self , options: .ignoreUnknownCharacters) {
            let dec = try AES(key: keyBytes, blockMode: blockMode).decrypt(data.bytes)
            let dec1 = dec.map { Int8(bitPattern: $0) }
            let decData = Data(bytes: UnsafePointer<Int8>(dec1), count: Int(dec1.count))
            
            if let result = NSString(data: decData, encoding: String.Encoding.utf8.rawValue){
                return String(result)
            }
            else{
                return ""
            }
        }
        else {
            return ""
        }
    }
}
