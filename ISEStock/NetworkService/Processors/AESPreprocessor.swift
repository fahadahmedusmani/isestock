//
//  AESPreprocessor.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//
import UIKit
import CryptoSwift

class AESPreprocessor: NSObject, RequestProcessorDelegate {
    
    func processRequest(_ request: Request) {
        
        let decodedKeyData = Data(base64Encoded: Encryption.aesKey, options: .ignoreUnknownCharacters)
        let decodedIVData = Data(base64Encoded: Encryption.aesIV)
        
        if var requestBody = request.body as? [String: Any]{
            
            for (key, value) in requestBody{
                
                let encryptedValue = try! (value as? String)?.aesEncrypt(
                    data: decodedKeyData!.bytes,
                    blockMode : CBC(iv: decodedIVData!.bytes)
                )
                
                requestBody[key] = encryptedValue
            }
            request.body = requestBody
        }
    }
}


extension String {
    
    func aesEncrypt(data: [UInt8], blockMode: BlockMode) throws -> String{
        let enc = try AES(key: data, blockMode: blockMode, padding: .pkcs7).encrypt(self.utf8.map({$0}))
        let enc1 = enc.map { Int8(bitPattern: $0) }
        let encData = Data(bytes: UnsafePointer<Int8>(enc1), count: Int(enc1.count))
        let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
        
        let result = String(base64String)
        return result
    }
}
