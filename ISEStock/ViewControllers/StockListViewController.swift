//
//  MenuViewController.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit
import SideMenu

class StockListViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var viewModel : StockListViewModel?
    
    let cellIdentifier = "listTableViewCell"
    let sectionHeaderIdentifier = "listTableViewSectionHeader"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = .red
        self.title = Title.stockAndIndices.rawValue
        registerCells()
        prepareData()
    }
    
    @IBAction func didClickMenuButton(_ sender: Any) {
        
        //PREPARING SIDE MENU TO BE PRESENTED
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MenuTableTableViewController") as! MenuTableTableViewController
        let menu = SideMenuNavigationController(rootViewController: vc)
        menu.leftSide = true
        menu.presentationStyle = .menuSlideIn
        present(menu, animated: true, completion: nil)
        
        vc.menuItemSelected = { [weak self] (period) in
            self?.viewModel?.fetchData(period: period)
        }
    }
    
    
    func prepareData(){
        searchBar.delegate = self
        viewModel = StockListViewModel.init(controller: self)
        viewModel?.updateData = { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    fileprivate func registerCells() {
        tableView.register(UINib.init(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.register(UINib.init(nibName: "ListTableViewSection", bundle: nil), forHeaderFooterViewReuseIdentifier: sectionHeaderIdentifier)
    }
    
}

//MARK:- Tableview delegates
extension StockListViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ListTableViewCell{
            
            if let stock = viewModel?.getDataFor(index: indexPath.row){
                cell.bindData(stock: stock, index: indexPath.row)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderIdentifier){
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StockDetailViewController") as! StockDetailViewController
        let vm = StockDetailViewModel.init(stockId: (viewModel?.getDataFor(index: indexPath.row))!.id!, controller: vc)
        vc.viewModel = vm
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- SEARCHBAR DELEGATE METHODS
extension StockListViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty{
            viewModel?.isSearching = false
            tableView.reloadData()
        }
        else{
            viewModel?.isSearching = true
            viewModel?.searchKeyWord(searchText.uppercased())
        }
    }
}
