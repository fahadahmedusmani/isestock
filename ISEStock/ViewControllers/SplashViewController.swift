//
//  LaunchScreenViewController.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import UIKit

class SplashViewController: UIViewController {
    
    var viewModel : SplashViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = SplashViewModel.init(controller: self)
        viewModel?.handshakeCompletedSuccessfully = {
            
            //PREPARE AND PUSH STOCK LIST VIEW CONTROLLER
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StockListViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
