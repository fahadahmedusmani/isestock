//
//  MenuTableTableViewController.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit

class MenuTableTableViewController: UITableViewController {

    let cellIdentifier = "menuTableViewCell"
    var menuItemSelected : ((_ value: AppConstant.Period) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableViewHeader()
    }
    
    func setTableViewHeader(){
        let view = MenuHeaderView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 155))
        tableView.tableHeaderView = view
    }

    // MARK: - Table view data source and delegates methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppConstant.sideMenuData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = AppConstant.sideMenuData[indexPath.row].rawValue
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        self.menuItemSelected?(AppConstant.Period.getPeriod(for: AppConstant.sideMenuData[indexPath.row]))
        self.dismiss(animated: true)
    }
}
