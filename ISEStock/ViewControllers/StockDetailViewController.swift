//
//  StockDetailViewController.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import UIKit
import Charts

class StockDetailViewController: UIViewController {
    
    let cellIdentifier = "stockDetailCell"
    var viewModel : StockDetailViewModel?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chartView: LineChartView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = .red
        self.title = Title.stockAndIndices.rawValue
        registerCells()
        setupChartView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel?.updateData = { [weak self] (chartData) in
            self?.chartView.data = chartData
            self?.tableView.reloadData()
        }
    }
    
    //SETTING UP CHART
    fileprivate func setupChartView(){
        chartView.backgroundColor = .systemGroupedBackground
        chartView.rightAxis.enabled = false
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.setLabelCount(6, force: false)
        chartView.leftAxis.setLabelCount(6, force: false)
        chartView.leftAxis.axisLineColor = .lightGray
        chartView.drawMarkers = true
        chartView.animate(xAxisDuration: 2.5)
    }
    
    //REGISTERING TABLE CELLS
    fileprivate func registerCells() {
        tableView.register(UINib.init(nibName: "StockDetailTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
}


//MARK:- Tableview delegates
extension StockDetailViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.getSectionCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getRowCount(for: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? StockDetailTableViewCell{
            
            if let data = viewModel?.getData(for: indexPath){
                cell.bindData(with: data)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
