//
//  StockListViewModel.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class StockListViewModel: BaseViewModelDelegate {
    
    var stockList : [Stock]?
    var filteredStockList : [Stock]?
    var isSearching: Bool?
    var controller : UIViewController
    var updateData : () -> () = {}
    
    init(controller: UIViewController) {
        self.controller = controller
        fetchData(period: .all)
    }
    
    //RETURN DATA TO BE DISPLAYED ON TABLEVIEW CELL
    func getDataFor(index: Int) -> Stock?{
        if isSearching ?? false{
            return filteredStockList?[index]
        }
        else{
            return stockList?[index]
        }
    }
    
    func getCount() -> Int{
        if isSearching ?? false{
            return filteredStockList?.count ?? 0
        }
        else{
            return stockList?.count ?? 0
        }
    }
    
    //FETCHING STOCK LIST BY PERIOD
    func fetchData(period: AppConstant.Period){
        
        showProgress(view: controller.view)
        
        StockNetworkService.getStockList(GetStockListRequest.init(period: period.rawValue)) { [weak self] (response, error) in
            
            self?.hideProgress()
            
            guard error == nil else{
                self?.showAlert(title: Title.alert.rawValue, message: error!, controller: self!.controller)
                return
            }
            self?.stockList = response?.stocks ?? []
            self?.updateData()
        }
    }
    
    //SEARCHING ALGORITHM IMPLEMENTED HERE
    func searchKeyWord(_ text: String){
        self.filteredStockList = []
        self.filteredStockList = stockList!.filter({ (stock) -> Bool in
            return (
                (stock.symbol?.contains(text))! ||
                    String(describing: stock.price ?? 0).uppercased().contains(text) ||
                    String(describing: stock.difference ?? 0).uppercased().contains(text) ||
                    String(describing: stock.volume ?? 0).uppercased().contains(text) ||
                    String(describing: stock.bid ?? 0).uppercased().contains(text) ||
                    String(describing: stock.offer ?? 0).uppercased().contains(text)
            )
        })
        self.updateData()
    }
}
