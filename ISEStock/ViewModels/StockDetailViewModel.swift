//
//  StockDetailViewModel.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import Foundation
import Charts

class StockDetailViewModel: BaseViewModelDelegate{
    
    var controller : UIViewController
    private var stock : Stock?
    private var data : [[String]]?
    private var chartData : LineChartData?
    var updateData : ((_ chartData: LineChartData) -> ())?
    
    init(stockId: Int, controller: UIViewController) {
        self.controller = controller
        fetchDetails(for: stockId)
    }
    
    func getData(for indexPath: IndexPath) -> [String]{
        return data?[indexPath.section] ?? []
    }
    
    func getSectionCount() -> Int{
        return self.data?.count ?? 0
    }
    
    func getRowCount(for section: Int) -> Int{
        return 1
    }
    
    func fetchDetails(for stockId: Int){
        
        showProgress(view: controller.view)
      
        StockNetworkService.getStockDetail(GetStockDetailRequest.init(id: String(stockId))) { [weak self] (response, error) in
            
            self?.hideProgress()
            
            guard error == nil else{
                self?.showAlert(title: Title.alert.rawValue, message: error!, controller: self!.controller)
                return
            }
            
            self?.stock = response!.stock!
            self?.prepareData()
            self?.prepareChartData()
        }
    }
    
    //PREPARING STOCK DETAILS TO BE DISPLAYED
    func prepareData(){
        data = []
        data?.append(["\(Title.symbol.rawValue): \(String(describing: stock?.symbol ?? ""))",
                      "\(Title.dailyMiscarriage.rawValue): \(String(describing: stock?.lowest ?? 0))"])
        
        data?.append(["\(Title.price.rawValue): \(String(describing: stock?.price ?? 0))",
                      "\(Title.dailyHigh.rawValue): \(String(describing: stock?.highest ?? 0))"])
        
        data?.append(["\(Title.difference.rawValue): \(String(describing: stock?.difference ?? 0))",
                      "\(Title.piece.rawValue): \(String(describing: stock?.minimum ?? 0))"])
        
        data?.append(["\(Title.volume.rawValue): \(String(describing: stock?.volume ?? 0))",
                      "\(Title.ceiling.rawValue): \(String(describing: stock?.maximum ?? 0))"])
        
        data?.append(["\(Title.buying.rawValue): \(String(describing: stock?.bid ?? 0))",
                      "\(Title.change.rawValue): \(String(describing: stock?.channge ?? 0))"])
        
        if stock?.isUp ?? false{
            data?.append(["\(Title.sales.rawValue): \(String(describing: stock?.offer ?? 0))",
                          "\(Title.change.rawValue): ▲"])
        }
        else{
            data?.append(["\(Title.sales.rawValue): \(String(describing: stock?.offer ?? 0))",
                          "\(Title.change.rawValue): ▼"])
        }
    }
    
    //PREPARING CHART DATA
    func prepareChartData(){
        var chartData : [ChartDataEntry] = []
        
        for graph in stock?.graph ?? []{
            chartData.append(ChartDataEntry.init(x: Double(graph.day ?? 0), y: graph.value ?? 0))
        }
        
        let dataSet = LineChartDataSet.init(entries: chartData, label: "Stocks")
        dataSet.drawCirclesEnabled = true
        dataSet.circleHoleColor = .black
        dataSet.circleColors = [.black]
        dataSet.circleRadius = .init(3.0)
        dataSet.lineWidth = 1.0
        dataSet.colors = [.black]
        dataSet.mode = .cubicBezier
        dataSet.fill = Fill.init(color: .red)
        dataSet.fillAlpha = 0.5
        dataSet.drawFilledEnabled = true
        
        self.chartData = LineChartData.init(dataSet: dataSet)
        self.chartData?.setDrawValues(false)
        self.updateData?(self.chartData!)
    }
}
