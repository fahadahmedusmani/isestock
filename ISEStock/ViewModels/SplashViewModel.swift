//
//  SplashViewModel.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation
import UIKit

class SplashViewModel: BaseViewModelDelegate {
    
    var controller : UIViewController
    var handshakeCompletedSuccessfully : (() -> ()) = {}
    
    init(controller: UIViewController) {
        self.controller = controller
        
        //Starting handshake with server
        startHandshakeWithServer()
    }
    
    func startHandshakeWithServer(){
        
        let device = UIDevice.current
        
        SecurityNetworkService.startHandshake(StartHandshakeRequest.init(deviceId: device.identifierForVendor?.description, systemVersion: device.systemVersion, platformName: device.systemName, deviceModel: device.name, manifacturer: device.model)) { [weak self] (response, error) in
            
            guard error == nil else{
                self?.showAlert(title: Title.alert.rawValue, message: error!, controller: self!.controller)
                return
            }
            
            guard let authorization = response?.authorization, let aesIV = response?.aesIV, let aesKey = response?.aesKey else{
                self?.showAlert(title: Title.alert.rawValue, message: error ?? "", controller: self!.controller)
                return
            }
            
            SessionManager.shared.validateSession(authorization: authorization)
            Encryption.aesIV = aesIV
            Encryption.aesKey = aesKey
            self?.handshakeCompletedSuccessfully()
        }
    }
}
