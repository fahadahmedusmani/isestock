//
//  Stock.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation
import CryptoSwift

class Stock : Codable{
    
    var id           : Int?
    var isDown       : Bool?
    var isUp         : Bool?
    var bid          : Double?
    var difference   : Double?
    var offer        : Double?
    var price        : Double?
    var volume       : Double?
    var symbol       : String?
    var highest      : Double?
    var lowest       : Double?
    var maximum      : Double?
    var minimum      : Double?
    var count        : Double?
    var channge      : Double?
    var graph        : [Graph]?
    
    
    enum CodingKeys : String, CodingKey{
        case id
        case isDown
        case isUp
        case bid
        case difference
        case offer
        case price
        case volume
        case symbol
        case graph = "graphicData"
        case highest
        case lowest
        case maximum
        case minimum
        case count
        case channge
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        isDown = try values.decodeIfPresent(Bool.self, forKey: .isDown)
        isUp = try values.decodeIfPresent(Bool.self, forKey: .isUp)
        bid = try values.decodeIfPresent(Double.self, forKey: .bid)
        difference = try values.decodeIfPresent(Double.self, forKey: .difference)
        offer = try values.decodeIfPresent(Double.self, forKey: .offer)
        price = try values.decodeIfPresent(Double.self, forKey: .price)
        volume = try values.decodeIfPresent(Double.self, forKey: .volume)
        graph = try values.decodeIfPresent([Graph].self, forKey: .graph)
        highest = try values.decodeIfPresent(Double.self, forKey: .highest)
        lowest = try values.decodeIfPresent(Double.self, forKey: .lowest)
        maximum = try values.decodeIfPresent(Double.self, forKey: .maximum)
        minimum = try values.decodeIfPresent(Double.self, forKey: .minimum)
        count = try values.decodeIfPresent(Double.self, forKey: .count)
        channge = try values.decodeIfPresent(Double.self, forKey: .channge)

        //decrypt symbol value
        if let symbol = try values.decodeIfPresent(String.self, forKey: .symbol){
            
            let decodedKeyData = Data(base64Encoded: Encryption.aesKey, options: .ignoreUnknownCharacters)
            let decodedIVData = Data(base64Encoded: Encryption.aesIV)
            
            self.symbol =  try symbol.aesDecrypt(keyBytes: decodedKeyData!.bytes, blockMode: CBC(iv: decodedIVData!.bytes))
        }
    }
}
