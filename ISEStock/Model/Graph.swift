//
//  Graph.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import Foundation

class Graph : Codable{
    
    var day           : Int?
    var value         : Double?
}
