//
//  ListItemView.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit

class ListItemView: UIView {

    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var differenceLabel: UILabel!
    @IBOutlet weak var transactionVolumeLabel: UILabel!
    @IBOutlet weak var buyingLabel: UILabel!
    @IBOutlet weak var salesLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    internal func loadNib() {
        let view = getNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    internal func getNib() -> UIView {
        let bundle = Bundle(for: ListItemView.self)
        let nib = UINib(nibName: "ListItemView", bundle: bundle)
        guard let listItemView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        return listItemView
    }
    
    func setData(){
        
    }
}
