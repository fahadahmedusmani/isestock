//
//  ListTableViewCell.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var itemView: ListItemView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func bindData(stock: Stock, index: Int){
        updateBackgroundColor(index: index)
        setData(stock: stock)
    }
    
    func updateBackgroundColor(index: Int){
        if index % 2 == 0{
            self.backgroundColor = .white
        }
        else{
            self.backgroundColor = .systemGroupedBackground
        }
    }
    
    func setData(stock: Stock){
        itemView.buyingLabel.text = String(describing: stock.bid ?? 0)
        itemView.differenceLabel.text = String(describing: stock.difference ?? 0)
        itemView.priceLabel.text = String(describing: stock.price ?? 0)
        itemView.salesLabel.text = String(describing: stock.offer ?? 0)
        itemView.transactionVolumeLabel.text = String(describing: stock.volume ?? 0)
        itemView.symbolLabel.text = stock.symbol
        
        if stock.isUp ?? false{
            itemView.changeLabel.text = "▲"
            itemView.changeLabel.textColor = .green
        }
        else{
            itemView.changeLabel.text = "▼"
            itemView.changeLabel.textColor = .red
        }
    }
    
}
