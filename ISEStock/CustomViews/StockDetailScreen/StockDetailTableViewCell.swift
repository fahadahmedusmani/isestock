//
//  StockDetailTableViewCell.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import UIKit

class StockDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func bindData(with detail: [String]){
        leftLabel.text = detail.first
        rightLabel.text = detail.last
        
        if rightLabel.text?.contains("▲") ?? false{
            rightLabel.textColor = .green
        }
        else if rightLabel.text?.contains("▼") ?? false{
            rightLabel.textColor = .red
        }
    }
}
