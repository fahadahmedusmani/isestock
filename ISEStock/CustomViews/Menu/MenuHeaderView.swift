//
//  MenuHeaderView.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import UIKit

class MenuHeaderView: UIView {    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    internal func loadNib() {
        let view = getNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    internal func getNib() -> UIView {
        let bundle = Bundle(for: MenuHeaderView.self)
        let nib = UINib(nibName: "MenuHeaderView", bundle: bundle)
        guard let headerView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        return headerView
    }
}
