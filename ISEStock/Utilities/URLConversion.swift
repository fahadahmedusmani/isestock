//
//  URLConversion.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

class URLConversion {
    
    static func decryptURL ( from value : [UInt8] ) -> String? {
        
        let utf8 = String(data: Data(value), encoding: .utf8) ?? ""
        let base64 = String(data: Data( base64Encoded: utf8) ?? Data() , encoding: .utf8)
        return base64
    }
}
