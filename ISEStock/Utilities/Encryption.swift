//
//  Encryption.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation

struct Encryption {
    
    /*** Encryption Decryption keys ***/
    static var aesKey : String = ""
    static var aesIV  : String = ""

    /*** Use to encrypt and decrypt webservice payload ***/
    static func isEncryptionEnabled() -> Bool {
        switch AppConstant.isEncryptionEnabled{
        case true:
            return true
        case false:
            return false
        }
    }

}
