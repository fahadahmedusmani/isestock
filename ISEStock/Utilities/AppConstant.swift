//
//  AppConfiguration.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

struct AppConstant{
    
    static var isEncryptionEnabled : Bool = true
    static var sideMenuData : [Menu] = [.stocksandIndices, .iSERisers, .iMKBFallers, .iMKB30, .iMKB50, .iMKB100]
    
    enum Menu: String{
        case stocksandIndices = "Stocks and Indices"
        case iSERisers = "ISE Risers"
        case iMKBFallers = "IMKB Fallers"
        case iMKB30 = "IMKB-30"
        case iMKB50 = "IMKB-50"
        case iMKB100 = "IMKB-100"
    }
    
    enum Period: String {
        case all
        case increasing
        case decreasing
        case volume30
        case volume50
        case volume100
        
        static func getPeriod(for menu: Menu) -> Period{
            
            switch menu {
            case .stocksandIndices:
                return .all
            case .iSERisers:
                return .increasing
            case .iMKBFallers:
                return .decreasing
            case .iMKB30:
                return .volume30
            case .iMKB50:
                return .volume50
            case .iMKB100:
                return .volume100
            }
        }
    }
}
