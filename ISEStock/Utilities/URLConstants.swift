//
//  URLConstants.swift
//  ISEStock
//
//  Created by Avanza on 28/05/2021.
//

import Foundation

class URLConstants {
    
    static func getBaseUrl() -> String {
        return "https://mobilechallenge.veripark.com"
    }
    
    struct EndPoints {
        static let startHandshake : [UInt8] = [76, 50, 70, 119, 97, 83, 57, 111, 89, 87, 53, 107, 99, 50, 104, 104, 97, 50, 85, 118, 99, 51, 82, 104, 99, 110, 81, 61]

        static let getStockList : [UInt8] = [76, 50, 70, 119, 97, 83, 57, 122, 100, 71, 57, 106, 97, 51, 77, 118, 98, 71, 108, 122, 100, 65, 61, 61]

        
        static let getStockDetail : [UInt8] = [76, 50, 70, 119, 97, 83, 57, 122, 100, 71, 57, 106, 97, 51, 77, 118, 90, 71, 86, 48, 89, 87, 108, 115]

    }
}
