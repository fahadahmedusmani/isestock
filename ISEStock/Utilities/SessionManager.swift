//
//  SessionManager.swift
//  ISEStock
//
//  Created by Avanza on 29/05/2021.
//

import Foundation

class SessionManager{
    
    static var shared = SessionManager.init()
    private var authorization : String?
    
    private init(){}
    
    //use this func for session timer and token validations
    func validateSession(authorization: String){
        self.authorization = authorization
    }
    
    func getAuthorization() -> String?{
        return self.authorization ?? ""
    }
}
