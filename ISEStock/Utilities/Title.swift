//
//  Title.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import Foundation

enum Title : String {
    case requestFailed = "Unable to process request at the moment. Please try later."
    case stockAndIndices = "Stock And Indices"
    case alert = "Alert"
    case ok
    case symbol = "Symbol"
    case dailyMiscarriage = "Daily Miscarriage"
    case price = "Price"
    case dailyHigh = "Daily High"
    case difference = "%difference"
    case piece = "Piece"
    case volume = "Volume"
    case ceiling = "Ceiling"
    case buying = "Buying"
    case sales = "Sales"
    case change = "Change"
}
