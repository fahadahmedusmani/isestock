//
//  BaseViewModelDelegate.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//
import Foundation
import UIKit
import NVActivityIndicatorView

protocol BaseViewModelDelegate {
    func showAlert(title:String, message: String, controller: UIViewController)
    func showProgress(view: UIView)
    func hideProgress()
}


extension BaseViewModelDelegate{
    
   
    func showProgress(view: UIView){
        ActivityService.showProgress(view: view)
    }
    
    func hideProgress(){
        ActivityService.hideProgress()
    }
    
    func showAlert(title: String, message: String, controller: UIViewController) {
        AlertService.sharedInstance.showAlert(title: title, message: message, buttonText: Title.ok.rawValue, controller: controller)
    }
}
