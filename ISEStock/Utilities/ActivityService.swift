//
//  ActivityService.swift
//  ISEStock
//
//  Created by Avanza on 30/05/2021.
//

import Foundation
import NVActivityIndicatorView

class ActivityService{
    
    static let activityIndicator = NVActivityIndicatorView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 50.0, height: 50.0), type: .circleStrokeSpin, color: .red)
    
    static func showProgress(view: UIView){
        activityIndicator.frame.origin.x = view.center.x - 25
        activityIndicator.frame.origin.y = view.center.y - 25
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        view.addSubview(activityIndicator)
    }
    
    static func hideProgress(){
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        activityIndicator.removeFromSuperview()
    }
}
